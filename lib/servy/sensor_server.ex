defmodule Servy.SensorServer do
  @name :sensor_server
  @refresh_interval :timer.seconds(500)

  use GenServer

  #
  # Client
  #
  def start_link(_arg) do
    IO.puts "Starting the sensor server..."
    GenServer.start_link(__MODULE__, %{}, name: @name)
  end

  def get_sensor_data do
    GenServer.call @name, :get_sensor_data
  end

  #
  # Server Callbacks
  #
  def init(_state) do
    initial_state = run_task_to_get_sensor_data()
    schedule_refresh()
    {:ok, initial_state}
  end

  def handle_info(:refresh, _state) do
    IO.puts "Refreshing cache"
    new_state = run_task_to_get_sensor_data()
    schedule_refresh()
    {:noreply, new_state}
  end

  def handle_call(:get_sensor_data, _from, state) do
    {:reply, state, state}
  end

  def run_task_to_get_sensor_data do
    snapshots = 1..3
    |> Enum.map(&Task.async(fn -> Servy.VideoCam.get_snapshot("cam-#{&1}") end))
    |> Enum.map(&Task.await/1)
    where_is_bigfoot =
      Task.async(fn -> Servy.Tracker.get_location("bigfoot") end)
      |> Task.await

    %{snapshots: snapshots, location: where_is_bigfoot}
  end

  defp schedule_refresh do
    Process.send_after(self(), :refresh, @refresh_interval)
  end
end
