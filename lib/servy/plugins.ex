defmodule Servy.Plugins do
  @moduledoc """
  Plugins for the Servy.Handler request transforming
  """
  require Logger
  alias Servy.Conv

  @doc """
  rewrite_path - Modifies certain paths into paths that are handled

  /wildlife => /wildthings
  /bears?id={id} => /bears/{id}
  """
  def rewrite_path(%Conv{path: "/wildlife"} = conv) do
    %Conv{ conv | path: "/wildthings" }
  end
  def rewrite_path(%Conv{path: "/bears?id=" <> id} = conv) do
    %Conv{ conv | path: "/bears/#{id}" }
  end
  def rewrite_path(conv), do: conv

  @doc """
  track - Log 4xx status requests
  """
  def track(%Conv{status: 404, path: path} = conv) do
    if Mix.env != :test do
      Logger.warn("Error: 404 #{path}")
    end
    conv
  end
  def track(%Conv{status: 403, path: path} = conv) do
    if Mix.env != :test do
      Logger.warn("Error: 403 #{path}")
    end
    conv
  end
  def track(%Conv{} = conv), do: conv

  @doc """
  log - Inspects the request with IO.inspect
  """
  def log(%Conv{} = conv) do
    if Mix.env == :dev do
      IO.inspect(conv)
    end
    conv
  end

  @doc """
  emojify - Adds happy emojis for 200 responses
  """
  def emojify(%Conv{status: 200, resp_body: body} = conv) do
    %Conv{ conv | resp_body: "😀\n#{body}\n😀" }
  end
  def emojify(%Conv{} = conv), do: conv
end
