defmodule Servy.Handler do
  @moduledoc """
  Handles requests to serve up responses
  """

  @pages_path Path.expand("../../pages", __DIR__)

  import Servy.Plugins, only: [rewrite_path: 1, track: 1, log: 1, emojify: 1]
  import Servy.Parser, only: [parse: 1]
  alias Servy.Conv
  alias Servy.BearController
  alias Servy.Api.BearController, as: ApiBearController

  @doc """
  handle takes a request and parses and transforms it before returning
  the response for the given request
  """
  def handle(request) do
    request
    |> parse
    |> rewrite_path
    |> log
    |> route
    |> track
    |> emojify
    |> format_response
  end

  @doc """
  route - Matches on the request method and path
  and adds a status and resp_body to the request
  """
  def route(%Conv{method: "POST", path: "/pledges"} = conv) do
    Servy.PledgeController.create(conv, conv.params)
  end
  def route(%Conv{method: "GET", path: "/pledges"} = conv) do
    Servy.PledgeController.index(conv)
  end
  def route(%Conv{method: "GET", path: "/wildthings"} = conv) do
    %Conv{ conv | status: 200, resp_body: "Bears, Lions, Tigers" }
  end
  def route(%Conv{method: "GET", path: "/sensors"} = conv) do
    sensor_data = Servy.SensorServer.get_sensor_data()
    %Conv{ conv | status: 200, resp_body: inspect(sensor_data) }
  end
  def route(%Conv{method: "GET", path: "/hibernate/" <> time} = conv) do
    time |> String.to_integer |> :timer.sleep
    %Conv{ conv | status: 200, resp_body: "Awake!" }
  end
  def route(%Conv{method: "GET", path: "/about"} = conv) do
    @pages_path
    |> Path.join("about.html")
    |> File.read
    |> handle_file(conv)
  end
  def route(%Conv{method: "GET", path: "/pages/" <> page} = conv) do
    @pages_path
    |> Path.join(page <> ".html")
    |> File.read
    |> handle_file(conv)
  end
  def route(%Conv{method: "GET", path: "/bears"} = conv) do
    BearController.index(conv)
  end
  def route(%Conv{method: "GET", path: "/api/bears"} = conv) do
    ApiBearController.index(conv)
  end
  def route(%Conv{method: "POST", path: "/bears"} = conv) do
    BearController.create(conv, conv.params)
  end
  def route(%Conv{method: "GET", path: "/bears/" <> id} = conv) do
    params = Map.put(conv.params, "id", id)
    BearController.show(conv, params)
  end
  def route(%Conv{method: "DELETE", path: "/bears/" <> _id} = conv) do
    BearController.destroy(conv, conv.params)
  end
  def route(%Conv{method: method, path: path} = conv) do
    %Conv{ conv | status: 404, resp_body: "No route matching #{method} #{path}" }
  end

  @doc """
  handle_file - File reading response matching to update the request with
  appropriate status and resp_body
  """
  def handle_file({:ok, content}, %Conv{} = conv) do
    %Conv{ conv | status: 200, resp_body: content}
  end
  def handle_file({:error, :enoent}, %Conv{} = conv) do
    %Conv{ conv | status: 404, resp_body: "File not found"}
  end
  def handle_file({:error, msg}, %Conv{} = conv) do
    %Conv{ conv | status: 500, resp_body: "Error: #{msg}"}
  end

  @doc """
  format_response - Takes the transformed request and builds a response
  """
  def format_response(%Conv{} = conv) do
    """
    HTTP/1.1 #{Conv.full_status(conv)}
    Content-Type: #{conv.resp_content_type}
    Content-Length: #{String.length(conv.resp_body)}

    #{conv.resp_body}
    """
  end
end
