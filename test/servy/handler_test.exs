defmodule Servy.HandlerTest do
    use ExUnit.Case, async: true

  test "GET /wildthings" do
    request = """
    GET /wildthings HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """
    response = Servy.Handler.handle(request)
    assert response == "HTTP/1.1 200 OK\nContent-Type: text/html\nContent-Length: 24\n\n😀\nBears, Lions, Tigers\n😀\n"
  end

  test "GET /bears" do
    request = """
    GET /bears HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    response = Servy.Handler.handle(request)
    assert response == "HTTP/1.1 200 OK\nContent-Type: text/html\nContent-Length: 359\n\n😀\n<h1>All the Bears!</h1>\n<ul>\n  \n    <li>Brutus - Grizzly</li>\n  \n    <li>Iceman - Polar</li>\n  \n    <li>Kenai - Grizzly</li>\n  \n    <li>Paddington - Brown</li>\n  \n    <li>Roscoe - Panda</li>\n  \n    <li>Rosie - Black</li>\n  \n    <li>Scarface - Grizzly</li>\n  \n    <li>Smokey - Black</li>\n  \n    <li>Snow - Polar</li>\n  \n    <li>Teddy - Brown</li>\n  \n</ul>\n\n😀\n"
  end

  test "GET /bigfoot" do
    request = """
    GET /bigfoot HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    response = Servy.Handler.handle(request)
    assert response == "HTTP/1.1 404 Not Found\nContent-Type: text/html\nContent-Length: 30\n\nNo route matching GET /bigfoot\n"
  end

  test "GET /bears/{id}" do
    request = """
    GET /bears/1 HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    response = Servy.Handler.handle(request)
    assert response == "HTTP/1.1 200 OK\nContent-Type: text/html\nContent-Length: 78\n\n😀\n<h1>Show Bear</h1>\n<p>\n  Is Teddy hibernating? <strong>true</strong>\n</p>\n\n😀\n"
  end

  test "GET /wildlife" do
    request = """
    GET /wildlife HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    response = Servy.Handler.handle(request)
    assert response == "HTTP/1.1 200 OK\nContent-Type: text/html\nContent-Length: 24\n\n😀\nBears, Lions, Tigers\n😀\n"
  end

  test "GET /bears?id={id}" do
    request = """
    GET /bears?id=4 HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    response = Servy.Handler.handle(request)
    assert response == "HTTP/1.1 200 OK\nContent-Type: text/html\nContent-Length: 81\n\n😀\n<h1>Show Bear</h1>\n<p>\n  Is Scarface hibernating? <strong>true</strong>\n</p>\n\n😀\n"
  end

  test "GET /about" do
    request = """
    GET /about HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    response = Servy.Handler.handle(request)
    assert response == "HTTP/1.1 200 OK\nContent-Type: text/html\nContent-Length: 664\n\n😀\n<!DOCTYPE html>\n<html>\n  <head>\n    <meta charset=\"utf-8\">\n    <title>About</title>\n  </head>\n  <body>\n    <h1>About The Wildlife Refuge</h1>\n    <p>\n      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\n      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\n      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\n      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat\n      non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n    </p>\n  </body>\n</html>\n\n😀\n"
  end

  test "GET /about-us" do
    request = """
    GET /about-us HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    response = Servy.Handler.handle(request)
    assert response == "HTTP/1.1 404 Not Found\nContent-Type: text/html\nContent-Length: 31\n\nNo route matching GET /about-us\n"
  end

  test "GET /pages/about" do
    request = """
    GET /pages/about HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    response = Servy.Handler.handle(request)
    assert response ==  "HTTP/1.1 200 OK\nContent-Type: text/html\nContent-Length: 664\n\n😀\n<!DOCTYPE html>\n<html>\n  <head>\n    <meta charset=\"utf-8\">\n    <title>About</title>\n  </head>\n  <body>\n    <h1>About The Wildlife Refuge</h1>\n    <p>\n      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\n      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\n      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\n      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat\n      non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n    </p>\n  </body>\n</html>\n\n😀\n"
  end

  test "POST /bears" do
    request = """
    POST /bears HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    Content-Type: application/x-www-form-urlencoded\r
    Content-Length: 21\r
    \r
    name=Baloo&type=Brown
    """

    response = Servy.Handler.handle(request)
    assert response == "HTTP/1.1 201 Created\nContent-Type: text/html\nContent-Length: 33\n\nCreated a Brown bear named Baloo!\n"
  end

  test "GET /api/bears" do
    request = """
    GET /api/bears HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    expected_response = "HTTP/1.1 200 OK\nContent-Type: application/json\nContent-Length: 609\n\n😀\n[{\"type\":\"Brown\",\"name\":\"Teddy\",\"id\":1,\"hibernating\":true},{\"type\":\"Black\",\"name\":\"Smokey\",\"id\":2,\"hibernating\":false},{\"type\":\"Brown\",\"name\":\"Paddington\",\"id\":3,\"hibernating\":false},{\"type\":\"Grizzly\",\"name\":\"Scarface\",\"id\":4,\"hibernating\":true},{\"type\":\"Polar\",\"name\":\"Snow\",\"id\":5,\"hibernating\":false},{\"type\":\"Grizzly\",\"name\":\"Brutus\",\"id\":6,\"hibernating\":false},{\"type\":\"Black\",\"name\":\"Rosie\",\"id\":7,\"hibernating\":true},{\"type\":\"Panda\",\"name\":\"Roscoe\",\"id\":8,\"hibernating\":false},{\"type\":\"Polar\",\"name\":\"Iceman\",\"id\":9,\"hibernating\":true},{\"type\":\"Grizzly\",\"name\":\"Kenai\",\"id\":10,\"hibernating\":false}]\n😀\n"
    response = Servy.Handler.handle(request)
    assert response == expected_response
  end
end
