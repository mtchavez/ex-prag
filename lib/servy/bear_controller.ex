defmodule Servy.BearController do
  alias Servy.Conv
  alias Servy.Wildthings
  alias Servy.Bear

  @templates_path Path.expand("../../templates", __DIR__)

  @doc """
  index route for GET /bears
  """
  def index(conv) do
    bears =
      Wildthings.list_bears()
      |> Enum.sort(&Bear.order_asc_by_name/2)
    %Conv{ conv | status: 200, resp_body: render("index", bears: bears) }
  end

  @doc """
  show route for GET /bears/{id}
  """
  def show(conv, %{"id" => id}) do
    bear = Wildthings.get_bear(id)
    %Conv{ conv | status: 200, resp_body: render("show", bear: bear)}
  end

  @doc """
  create route for POST /bears
  """
  def create(conv, %{"name" => name, "type" => type}) do
    %Conv{ conv | status: 201, resp_body: "Created a #{type} bear named #{name}!" }
  end

  @doc """
  destroy route for DELETE /bears/{id}

  403 FORBIDDEN
  """
  def destroy(conv, _params) do
    %Conv{ conv | status: 403, resp_body: "Deleting bears is forbidden"}
  end

  defp render(template, bindings) do
    @templates_path
    |> Path.join(template <> ".eex")
    |> EEx.eval_file(bindings)
  end
end
