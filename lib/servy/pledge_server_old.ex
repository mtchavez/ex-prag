defmodule Servy.GenericServer do
  #
  # Client
  #
  def start(callback_module, initial_state, name) do
    IO.puts "Starting the pledge server..."
    pid = spawn(__MODULE__, :listen_loop, [callback_module, initial_state])
    Process.register(pid, name)
    pid
  end

  def call(pid, message) do
    send pid, {:call, self(), message}
    receive do {:response, response} -> response end
  end

  def cast(pid, message) do
    send pid, {:cast, message}
  end

  #
  # Server
  #
  def listen_loop(callback_module, state \\ []) do
    receive do
      {:call, sender, message} when is_pid(sender) ->
        {response, new_state} = callback_module.handle_call(message, state)
        send sender, {:response, response}
        listen_loop(callback_module, new_state)
      {:cast, message} ->
        new_state = callback_module.handle_cast(message, state)
        listen_loop(callback_module, new_state)
      unexpected ->
        IO.puts "Unexpected message: #{inspect unexpected}"
        listen_loop(callback_module, state)
    end
  end
end

defmodule Servy.PledgeServerOld do

  @name :pledge_server_old
  alias Servy.GenericServer

  def start do
    IO.puts "Starting PlegeServer."
    GenericServer.start(__MODULE__, [], @name)
  end
  def create_pledge(name, amount) do
    GenericServer.call @name, {:create_pledge, name, amount}
  end

  def recent_pledges do
    GenericServer.call @name, :recent_pledges
  end

  def total_pledged do
    GenericServer.call @name, :total_pledged
  end

  def clear do
    GenericServer.cast @name, :clear
  end

  #
  # Server Callbacks
  #
  def handle_cast(:clear, _state) do
    []
  end

  def handle_call(:total_pledges, state) do
    total = Enum.reduce(state, 0, fn(pledge, tot) -> elem(pledge, 1) + tot end)
    {total, state}
  end

  def handle_call(:recent_pledges, state) do
    {state, state}
  end

  def handle_call({:create_pledges, name, amount}, state) do
    {:ok, id} = send_pledge_to_service(name, amount)
    most_recent = Enum.take(state, 2)
    new_state = [ {name, amount} | most_recent ]
    {id, new_state}
  end

  defp send_pledge_to_service(_name, _amount) do
    {:ok, "pledge-#{:rand.uniform(1000)}"}
  end
end
