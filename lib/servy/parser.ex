defmodule Servy.Parser do
  @moduledoc """
  Parser handling for requests in Servy.Handler
  """
  alias Servy.Conv

  @doc ~S"""
  parse - Parses the request and passes on information
  to transform into a response

  ## Examples

    iex> request = "POST /bears HTTP/1.1\r\nHost: example.com\r\nUser-Agent: ExampleBrowser/1.0\r\nAccept: */*\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 21\r\n\r\nname=Baloo&type=Brown"
    iex> Servy.Parser.parse(request)
    %Servy.Conv{
      method: "POST",
      path: "/bears",
      params: %{"name" => "Baloo", "type" => "Brown"},
      headers: %{"Accept" => "*/*", "Content-Length" => "21",
              "Content-Type" => "application/x-www-form-urlencoded",
              "Host" => "example.com", "User-Agent" => "ExampleBrowser/1.0"}
    }
  """
  def parse(request) do
    [top, params_string] = String.split(request, "\r\n\r\n")
    [request_line | header_lines] = String.split(top, "\r\n")
    [method, path, _ ] = String.split(request_line, " ")
    headers = parse_headers(header_lines)
    %Conv{
      method: method,
      path: path,
      params: parse_params(headers["Content-Type"], params_string),
      headers: headers
    }
  end

  defp parse_params("application/x-www-form-urlencoded", params) do
    params
    |> String.trim
    |> URI.decode_query
  end
  defp parse_params(_, _), do: %{}

  @doc ~S"""
   Parses headers into a map

  ## Examples

    iex> headers = String.split("Host: example.com\r\nUser-Agent: ExampleBrowser/1.0\r\nAccept: */*\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 21", "\r\n")
    iex> Servy.Parser.parse_headers(headers)
    %{"Accept" => "*/*", "Content-Length" => "21",
             "Content-Type" => "application/x-www-form-urlencoded",
             "Host" => "example.com", "User-Agent" => "ExampleBrowser/1.0"}
  """
  def parse_headers(header_lines) do
    header_lines
    |> Enum.reduce(%{}, fn (line, headers) ->
      [key, val] = String.split(line, ": ")
      Map.put(headers, key, val)
    end)
  end
end
